# Ansible Linux

The Ansible role `setup` is used to configure either MXLinux 23 or more recently, LMDE 6 with a common set of utilities and programs.

The `essential` tag installs a barebones set of packages that can be used on a remote SSH terminal or WSL. The `system` tag is for installing a new vanilla Debian based system with common tools and packages.

## Installation

Use the commands below to install Ansible and "dry-run" the ansible role on a newly install system:

```sh
sudo apt install git ansible -y
mkdir ~/src && cd ~/src
git clone https://gitlab.com/dolby/ansible-linux
cd ansible-linux
ansible-playbook setup.yml -C -K -t essential
```

To setup `sudo`, `nvim` and install some essential utilities run:

```sh
ansible-playbook setup.yml -K -t essential
```

> [!NOTE]
> The `-K` flag is only needed if `sudo` has not been setup, normally on the first run.

To run a full installation of all programs and utilities run:

```sh
ansible-playbook setup.yml -t system
```

## Issues

### Swapping CapsLock and Escape keys in LMDE's Cinnamon DE

This is done by updating `/etc/defaults/keyboard`, unfortunately Cinammon does not honour this and require the swap to be set in the DE.

## Todo

- Install Syncthing with Ansible
- Install Stow files via Ansible
- Fix `user` and `home_dir` vars in code
- Opening Nvim via the desktop icon gives a path error
